struct config  {
	int pixelCount;
	uint32_t forgroundPixelColor;
	uint32_t backgroundPixelColor;
};

class ColorWipe {
  private:
    int _currentPixel;
    config _config;
  
  public:
    ColorWipe(config config);
    std::vector<uint32_t> tick(void);
    void setPixelColor(uint32_t newColor);
};
