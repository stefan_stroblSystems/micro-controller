/*
 * Libraries
 */
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>

#include <ArduinoJson.h>

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
#include <avr/power.h>
#endif

#include "ColorWipe.h"

//
/*
 * Constants
 */
// const char* ssid = "FabLab LG2.4Ghz";
// const char* password = "fablablg2018+2";
const char* ssid = "isHome";
const char* password = "ZRbokfO37ZTeoOwL";

const int led = 2;

const int NEO_PIXEL_PIN = D5;
const int NUMBER_OF_PIXEL_IN_STRIP = 60;

/*
 * Global variables
 */
//
// === Server
ESP8266WebServer server(80);
bool ledState = false;

//
// === NeoPixel
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUMBER_OF_PIXEL_IN_STRIP,
		NEO_PIXEL_PIN, NEO_GRB + NEO_KHZ800);

int neoPixelProgramm = 1; // "RgbColorWipe"
unsigned long previousMillis = 0;
const long tickInterval = 200;

ColorWipe colorWipe(NUMBER_OF_PIXEL_IN_STRIP, strip.Color(0, 255, 0));

//
/*
 * Setup
 */
void setup(void) {
	// === Setup Serial bus
	Serial.begin(115200);

	//
	// === Setup Led output Pin
	pinMode(led, OUTPUT);
	digitalWrite(led, 0);

	//
	// == Setup NeoPixel
	strip.begin();
	strip.show(); // Initialize all pixels to 'off'

	//
	// == Setup WiFi
	WiFi.mode(WIFI_STA);
	WiFi.begin(ssid, password);
	Serial.println("");

	//
	// === Wait for connection
	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
		Serial.print(".");
	}
	Serial.println("");
	Serial.print("Connected to ");
	Serial.println(ssid);
	Serial.print("IP address: ");
	Serial.println(WiFi.localIP());

	if (MDNS.begin("esp8266")) {
		Serial.println("MDNS responder started");
	}

	//
	// === Routing
	server.on("/", handleRoot);
	server.on("/led/on", []() {handleLedStateChange(true);});
	server.on("/led/off", []() {handleLedStateChange(false);});
	server.on("/strip", handleStripe);
	server.onNotFound(handleNotFound);

	//
	// ==0  Start server
	server.begin();
	Serial.println("HTTP server started");
}

void loop(void) {
	Serial.println("Loop");
	server.handleClient();

	unsigned long currentMillis = millis();
	unsigned long deltaTime = currentMillis - previousMillis;
	if (deltaTime >= tickInterval) {
		previousMillis = currentMillis;
		handleNeopixelProgramm();
	}
}

void handleNeopixelProgramm() {
	std::vector<uint32_t> stripeState;

	switch (neoPixelProgramm) {
	case 1:
		colorWipe.tick();
	}
}

void handleLedStateChange(bool state) {
	Serial.println("handleLedStateChange: " + bool2Json(state));
	ledState = state;
	digitalWrite(led, ledState);
	sendState();
}

void handleRoot() {
	Serial.println("handleRoot");
	sendState();
}

void handleStripe() {
	/*
	 if (server.method() == HTTP_GET) {
	 colorWipe(strip.Color(255, 0, 0), 50); // Red
	 colorWipe(strip.Color(0, 255, 0), 50); // Green
	 colorWipe(strip.Color(0, 0, 255), 50); // Blue
	 colorWipe(strip.Color(0, 0, 0), 50); // Blue

	 } else if (server.method() == HTTP_POST) {
	 StaticJsonBuffer < 200 > jBuffer;

	 String data = server.arg("plain");
	 JsonObject& jObject = jBuffer.parseObject(data);

	 String jsonStr;
	 jObject.printTo(jsonStr);
	 Serial.println(jsonStr);

	 String value1 = jObject["test"];
	 Serial.println(value1);
	 }

	 sendState();
	 */
}

void handleNotFound() {
	digitalWrite(led, 1);
	String message = "File Not Found\n\n";
	message += "URI: ";
	message += server.uri();
	message += "\nMethod: ";
	message += (server.method() == HTTP_GET) ? "GET" : "POST";
	message += "\nArguments: ";
	message += server.args();
	message += "\n";
	for (uint8_t i = 0; i < server.args(); i++) {
		message += " " + server.argName(i) + ": " + server.arg(i) + "\n";
	}
	server.send(404, "text/plain", message);
	digitalWrite(led, 0);
}

void sendState() {
	server.sendHeader("Access-Control-Allow-Origin", "*");
	server.send(200, "application/json", getResultJson());
}

String getResultJson() {
	return "{\"led\": " + bool2Json(ledState) + "}";
}

String bool2Json(bool flag) {
	if (flag) {
		return "true";
	} else {
		return "false";
	}
}

