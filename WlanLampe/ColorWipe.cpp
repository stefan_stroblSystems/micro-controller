#include <Arduino.h>
#include "ColorWipe.h"

ColorWipe::ColorWipe(config config) {
	_currentPixel = 0;
	_config = config;
}

std::vector<uint32_t> ColorWipe::tick(void) {
	std::vector<uint32_t> colorSchema[_config.pixelCount];

	_currentPixel = ((_currentPixel + 1) % _config.pixelCount);

	for (int i = 0; i < _config.pixelCount; i++) {
		if ( (i <= _currentPixel)){
			colorSchema[i] = _config.forgroundPixelColor;
		} else {
			colorSchema[i] = _config.backgroundPixelColor;
		}
	}

	return colorSchema;
}

void ColorWipe::setPixelColor(uint32_t newColor) {
	_config.forgroundPixelColor = newColor;
}
